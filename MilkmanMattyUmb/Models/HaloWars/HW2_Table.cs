﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace MilkmanMatty.Models.HaloWars {

	 [TableName("HaloWars2")]
	 [PrimaryKey("BuildId", autoIncrement = true)]
	 [ExplicitColumns]
	 public class HW2_Table {

		[Column("BuildId")]
		[PrimaryKeyColumn(AutoIncrement = true)]
		public int BuildId { get; set; }

		[Column("URL_Hash")]
		public string URL_Hash { get; set; }

		[Column("Order")]
		[NullSetting(NullSetting = NullSettings.Null)]
		public string Order { get; set; }

		[Column("Image")]
		[NullSetting(NullSetting = NullSettings.Null)]
		public string Img { get; set; }

		[Column("ActionType")]
		[NullSetting(NullSetting = NullSettings.Null)]
		public string ActionType { get; set; }

		[Column("Description")]
		[NullSetting(NullSetting = NullSettings.Null)]
		public string Description { get; set; }

		[Column("ActionTime")]
		[NullSetting(NullSetting = NullSettings.Null)]
		public string ActionTime { get; set; }

		[Column("DateCreated")]
		public DateTime Created { get; set; }

		[Column("Expiry")]
		public DateTime Expiry { get; set; }

		[Column("Locked")]
		public int Locked { get; set; }
	}
}