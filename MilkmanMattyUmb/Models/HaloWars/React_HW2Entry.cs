﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MilkmanMatty.Models.HaloWars {
	public class React_HW2Entry {
		public string actionType { get; set; }
		public string description { get; set; }
		public string img { get; set; }
		public string time { get; set; }
		public string URL_Hash { get; set; }
	}
}