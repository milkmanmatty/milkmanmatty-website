﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using MilkmanMatty.Models.HaloWars;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Web.WebApi;

namespace MilkmanMatty.Controllers {
	public class DBController : UmbracoApiController {

		DatabaseContext dc = ApplicationContext.Current.DatabaseContext;

		#region HW2 Build-Actions

		/// <summary>
		/// Get Build-Action entry by BuildId
		/// </summary>
		/// <param name="id">BuildId to look for</param>
		/// <returns>A HW2_Entry which is the Build-Action entry</returns>
		[HttpGet]
		public HW2_Table GetById(int id) {

			var query = new Sql().Select("*")
						.From<HW2_Table>(dc.SqlSyntax)
						.Where<HW2_Table>(x => x.BuildId == id, dc.SqlSyntax);

			var entry = dc.Database.Fetch<HW2_Table>(query).FirstOrDefault();

			//return Json(entry);
			return entry;
		}


		/// <summary>
		/// Gets All Entries with the hash specified.
		/// </summary>
		/// <param name="hash">content with this hash with be returned</param>
		/// <returns>
		///		A JSON serialzed List of HW2_Table Entries. The first entry is the Build, the following entries are the Build-Actions.
		/// </returns>
		[HttpGet()]
		public Object GetAllBuildData(string hash) {

			if(dc.SqlSyntax == null){
				dc = ApplicationContext.Current.DatabaseContext;
			}

			//First grab the Build.
			var query = new Sql().Select("*")
						.From<HW2_Table>(dc.SqlSyntax)
						.Where<HW2_Table>(x => x.ActionType == null && x.URL_Hash == hash, dc.SqlSyntax);

			List<HW2_Table> result = dc.Database.Fetch<HW2_Table>(query);

			//Next grab the all of the Build's Build-Actions.
			query = new Sql().Select("*")
					.From<HW2_Table>(dc.SqlSyntax)
					.Where<HW2_Table>(x => x.ActionType != null && x.URL_Hash == hash, dc.SqlSyntax)
					.OrderBy<HW2_Table>(x => x.Created, dc.SqlSyntax);

			result.AddRange(dc.Database.Fetch<HW2_Table>(query));

			return Content(HttpStatusCode.OK, JsonConvert.SerializeObject(result), new System.Net.Http.Formatting.JsonMediaTypeFormatter());
		}

		
		[HttpPost]
		[EnableCors(origins: "*", headers: "*", methods: "*")]
		public Object CreateOrUpdate(React_HW2Entry entry) {

			if(entry == null){
				return Content(HttpStatusCode.BadRequest, "Missing Param", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			} 

			HW2_Table newEntry = new HW2_Table(){
				Img = entry.img,
				ActionType = entry.actionType,
				Description = entry.description,
				ActionTime = entry.time,
				URL_Hash = entry.URL_Hash
			};

			if(CreateOrUpdate(newEntry)){
				return Content(HttpStatusCode.Created, JsonConvert.SerializeObject(entry), new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			}
			return Content(HttpStatusCode.Forbidden, "This one's Great Journey has ended", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
		}

		[HttpPost]
		[EnableCors(origins: "*", headers: "*", methods: "*")]
		public Object Lock(React_HW2Entry entry) {

			if(entry == null || String.IsNullOrWhiteSpace(entry.URL_Hash)){
				return Content(HttpStatusCode.BadRequest, "Missing Param", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			}

			var hash = entry.URL_Hash;

			if(dc.SqlSyntax == null){
				dc = ApplicationContext.Current.DatabaseContext;
			}

			//Grab the Build.
			var query = new Sql().Select("*")
						.From<HW2_Table>(dc.SqlSyntax)
						.Where<HW2_Table>(x => x.ActionType == null && x.URL_Hash == hash, dc.SqlSyntax);

			HW2_Table result = dc.Database.Fetch<HW2_Table>(query).FirstOrDefault();

			if(result != null && result.Locked != 1){
				result.Locked = 1;
				DatabaseContext.Database.Update(result);
				return Content(HttpStatusCode.OK, hash+" locked", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			} else {
				return Content(HttpStatusCode.InternalServerError, "Nope!", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			}
		}


		private bool CreateOrUpdate(HW2_Table entry) {
			if (entry != null) {

				//TODO - Add check for existing Build description

				if (entry.BuildId > 0) {
					DatabaseContext.Database.Update(entry);
				} else {
					entry.Created = DateTime.Now;

					//Grab the expiry of the parent Build entry if this is not a Build entry
					if(!String.IsNullOrEmpty(entry.ActionType)){

						if(String.IsNullOrWhiteSpace(entry.URL_Hash)){
							return false;
						}
						var b = GetBuild(entry.URL_Hash);
						if(b == null || b.Locked == 1){
							return false;
						}
						entry.Expiry= b.Expiry;
						DatabaseContext.Database.Save(entry);
					
					//Otherwise, this is a Build and it needs to have an exipry created
					} else {
						entry.Expiry = DateTime.Now.AddMonths(2);

						//TODO - Make call to function that will crawl and delete expired entries.

						var b = GetBuild(entry.URL_Hash);

						if(b != null && b.Locked == 0){
							DatabaseContext.Database.Update(entry);
						} else {
							DatabaseContext.Database.Save(entry);
						}
					}
				}
			}
			return true;
		}
	
		#endregion
		/*************************************************************************************************/
		/*************************************************************************************************/
		/*************************************************************************************************/
		#region HW2 Builds

		[HttpGet]
		public Object GetBuilds() {

			var query = new Sql().Select("*")
						.From<HW2_Table>(dc.SqlSyntax)
						.Where<HW2_Table>(x => String.IsNullOrWhiteSpace(x.ActionType), dc.SqlSyntax);

			var entry = dc.Database.Fetch<HW2_Table>(query);

			var Json = JsonConvert.SerializeObject(entry);

			return this.Content(HttpStatusCode.OK, Json, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
		}


		private HW2_Table GetBuild(string hash) {

			var query = new Sql().Select("*")
						.From<HW2_Table>(dc.SqlSyntax)
						.Where<HW2_Table>(x => (
								x.ActionType == null ||
								x.ActionType == ""
						) && x.URL_Hash == hash, dc.SqlSyntax);

			return dc.Database.Fetch<HW2_Table>(query).FirstOrDefault();
		}


		[HttpPost]
		[EnableCors(origins: "*", headers: "*", methods: "*")]
		public Object CreateBuild(React_HW2Entry entry) {

			if(entry == null){
				return Content(HttpStatusCode.BadRequest, "Missing Param", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			} else if(!String.IsNullOrWhiteSpace(entry.actionType) || String.IsNullOrEmpty(entry.description)){
				return Content(HttpStatusCode.BadRequest, "Bad Param", new System.Net.Http.Formatting.JsonMediaTypeFormatter());
			}


			string stamp = DateTime.Now.ToString("yyMMdHHmss");

			SHA256 sha = SHA256Managed.Create();
			byte[] hashValue = null;
			using(var stream = GenerateStreamFromString(stamp+entry.description)){
				hashValue = sha.ComputeHash(stream);
			}

			string hash = HttpServerUtility.UrlTokenEncode(hashValue);

			HW2_Table newEntry = new HW2_Table(){
				Description = entry.description,
				Locked = 0,
				URL_Hash = hash
			};

			CreateOrUpdate(newEntry);

			return Content(HttpStatusCode.Created, hash, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
		}


		#endregion
		/*************************************************************************************************/
		/*************************************************************************************************/
		/*************************************************************************************************/
		#region Hashing


		public static Stream GenerateStreamFromString(string s){
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(s);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}


		private string GetSha256Hash(SHA256 shaHash, string input){
			// Convert the input string to a byte array and compute the hash.
			byte[] data = shaHash.ComputeHash(Encoding.UTF8.GetBytes(input));

			// Create a new Stringbuilder to collect the bytes
			// and create a string.
			StringBuilder sBuilder = new StringBuilder();

			// Loop through each byte of the hashed data 
			// and format each one as a hexadecimal string.
			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			// Return the hexadecimal string.
			return sBuilder.ToString();
		}

		#endregion

	}
}