﻿using Umbraco.Core;
using Umbraco.Core.Persistence;
using MilkmanMatty.Models.HaloWars;
using System.Web.Http;
using System;
using System.Web.Routing;
using Umbraco.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace MilkmanMatty.Controllers {
	public class DBStartup : ApplicationEventHandler {

		protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext){

			GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(
				new System.Net.Http.Formatting.RequestHeaderMapping(
					"Accept",
					"text/html",
					StringComparison.InvariantCultureIgnoreCase,
					true,
					"application/json"
				)
			);

			GlobalConfiguration.Configure(WebApiConfig.Register);

			// Custom route to MyProductController which will use a node with a specific ID as the
			// IPublishedContent for the current rendering page
			RouteTable.Routes.MapUmbracoRoute(
				"reactApp", 
				"halo-wars/halo-wars-2-builder/{*pathInfo}",
				new {
					controller = "React",
					action = "Index", 
					id = UrlParameter.Optional
				},
				new UmbracoVirtualNodeByIdRouteHandler(1135)
			);
			RouteTable.Routes.MapUmbracoRoute(
				"reactApp2", 
				"halo-wars-2/build-creator-for-hw2/{*pathInfo}",
				new {
					controller = "React",
					action = "Index", 
					id = UrlParameter.Optional
				},
				new UmbracoVirtualNodeByIdRouteHandler(1135)
			);

			//DatabaseContext ctx = ApplicationContext.Current.DatabaseContext;
			DatabaseContext ctx = applicationContext.DatabaseContext;

			DatabaseSchemaHelper dbSchema = new DatabaseSchemaHelper(ctx.Database, applicationContext.ProfilingLogger.Logger, ctx.SqlSyntax);

			if (!dbSchema.TableExist("HaloWars2")){
				dbSchema.CreateTable<HW2_Table>(false);
			}
        }

	}
}