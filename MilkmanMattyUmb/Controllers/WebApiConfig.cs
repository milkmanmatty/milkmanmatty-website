﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace MilkmanMatty.Controllers {

	public static class WebApiConfig{

		public static void Register(HttpConfiguration config){
			var corsAttr = new EnableCorsAttribute("*", "*", "*");
			config.EnableCors(corsAttr);
		}

	}
}