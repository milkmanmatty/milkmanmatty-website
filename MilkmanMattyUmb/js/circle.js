﻿(function(){

var canvas = document.querySelector("#icon");
var context = canvas.getContext("2d");

var rotation = 0;
var both = false;
var step = (Math.PI / 180 / 6);
var cont;

document.querySelector('.brand').addEventListener('mouseover', function(){
	step = (Math.PI / 180);
	cont = setInterval(blink,200);
});
document.querySelector('.brand').addEventListener('mouseout', function(){
	step = (Math.PI / 180 / 6);
	document.querySelectorAll('.brand span .f').forEach(e => e.classList.remove('f'));
	clearInterval(cont);
});

function update() {
	context.clearRect(0, 0, canvas.width, canvas.height);
	rotation += step;
	if(rotation % degrees_to_radians(180) <= .005){
		both = !both;
	}
	draw_circle();
	requestAnimationFrame(update);
}
update();

var bars = 30 + (5 * (Math.random() * 4));
var radius = 13;
var deg = 360 / bars;
function draw_circle() {
	for(var i = 0; i < bars; i++){
		var x = (canvas.width/2)+radius*Math.cos(degrees_to_radians(i*deg)+rotation);
		var y = (canvas.height/2)+radius*Math.sin(degrees_to_radians(i*deg)+rotation);
		draw_rectangle(x,y,1,13,i*deg, context);
	}
}
function draw_rectangle(x,y,w,h,deg, context){
	context.save();
	context.translate(x, y);
	if(both){
		context.rotate(degrees_to_radians(deg+90)+rotation);
	} else {
		context.rotate(degrees_to_radians(deg+90));
	}
	context.fillStyle = getComputedStyle(document.documentElement).getPropertyValue('--color-link-primary');
	context.fillRect(-1*(w/2), -1*(h/2), w, h);
	context.restore();
}
function degrees_to_radians(degrees){
	return degrees * Math.PI / 180;
}



function initChars(){
	var e = document.querySelector('.brand > span'),
		t = e.textContent,
		s = "";
	
	for (var i = 0; i < t.length; i++) {
		s += '<span>'+t.charAt(i)+'</span>';
	}
	e.innerHTML = s;
}
initChars();
function blink(){
	var e = document.querySelector('.brand > span');
	var r = Math.floor(Math.random()*e.children.length);
	e.children[r].classList.add('f');
	setTimeout(function(){e.children[r].classList.remove('f');},1000);
}

/* Handle menu button outline */
var noout;
document.querySelector("label.hamburger").addEventListener("click", function(){
	document.querySelector("label.hamburger").classList.add("nooutline")
	clearTimeout(noout);
	noout = setTimeout(function(){ document.querySelector("label.hamburger").classList.remove("nooutline") },1100)
});
/* Handle menu keypress */
document.querySelector("label.hamburger").addEventListener("keydown", function(e){
	if(e.code == "Enter" || e.keyCode  == 13 ||
		e.code == "Space" || e.keyCode == 32 ||
		e.code == "ArrowDown" || e.keyCode == 40
	){
		e.preventDefault();
		document.querySelector("label.hamburger").click();
	} else if (e.code == "Escape" || e.keyCode  == 27) {
		e.preventDefault();
		document.querySelector("#drop").checked = false;
	}
});
/* Handle menu-item keypress */
function handleMenuItem(e){
	if(e.code == "ArrowDown" || e.keyCode == 40){
		e.preventDefault();
		if(e.target.parentElement.nextElementSibling){
			e.target.parentElement.nextElementSibling.firstChild.focus();
			return;
		} else {
			e.target.parentElement.parentElement.firstElementChild.firstChild.focus();
		}
	} else if(e.code == "ArrowUp" || e.keyCode == 38){
		e.preventDefault();
		if(e.target.parentElement.previousElementSibling){
			e.target.parentElement.previousElementSibling.firstChild.focus();
			return;
		} else {
			e.target.parentElement.parentElement.lastElementChild.firstChild.focus();
		}
	} else if (e.code == "Escape" || e.keyCode  == 27 ||
				e.code == "ArrowLeft" || e.keyCode  == 37
	) {
		e.preventDefault();
		document.querySelector("#drop").checked = false;
		document.querySelector("label.hamburger").focus();
	}
}
var MenuLinks = document.querySelectorAll('.menu li a');
for (var i = 0; i < MenuLinks.length; i++) {
	MenuLinks[i].addEventListener("keydown", function(event){ handleMenuItem(event); });
}
/* skip content link */
document.getElementById('skip').addEventListener('click', function(e) {
	e.preventDefault();
	var target = document.querySelectorAll('h1, h2, h3, h4, h5, h6')[0];
	target.setAttribute('tabindex', "-1");
	target.focus();
});

})();