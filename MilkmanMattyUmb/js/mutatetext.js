var chars = "/\\<>[]{}_______âĕ€ӣ”-=+*^!?#";

var el = document.querySelector('#change');

var counter = 0;

function setText(newText){
	var currentText = el.textContent,
		maxLength = Math.max(currentText.length,newText.length),
		times = [],
		delay = [],
		maxTimes = -1;
	
	for(var i = 0; i < maxLength; i++){
		delay[i] = Math.floor(20*Math.random());
		times[i] = delay[i];
		times[i] += Math.random() > 0.33 ? Math.floor(5*Math.random()) : Math.floor(25*Math.random());
		maxTimes = Math.max(maxTimes,times[i]+delay[i]);
	}
	
	for(var i = 0; i < maxTimes; i++){
		update(el,times,delay,currentText,newText,i);
	}
}

function update(el,times,delay,oldText,newText,tick){
	setTimeout(function(){
		var switchText = "";
		for(var x = 0; x < times.length; x++){
			if(delay[x] <= tick){
				if(times[x] > tick+1){
					switchText += "<span class='fx'>"+chars[Math.floor(chars.length*Math.random())]+'</span>';
				} else {
					switchText += x >= newText.length ? "" : newText[x];
				}
			} else {
				switchText += oldText[x] || "";
			}
		}
		el.innerHTML = switchText;
	},tick*65 - Math.floor(50*Math.random()));
}

setInterval(cycleText,8000);