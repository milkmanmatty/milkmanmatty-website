﻿(function () {
	var scene = new THREE.Scene();

	// create a camera, which defines where we're looking at.
	var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);

	// create a render and set the size
	var webGLRenderer = new THREE.WebGLRenderer();
	webGLRenderer.setClearColor(new THREE.Color(0x000000, 1.0));

	webGLRenderer.setSize(getWindowWidth(), getWindowHeight());
	webGLRenderer.shadowMapEnabled = true;

	window.addEventListener("resize", function(){
		webGLRenderer.setSize(getWindowWidth(), getWindowHeight());
	});

	// position and point the camera to the center of the scene
	camera.position.x = -30;
	camera.position.y = 40;
	camera.position.z = 50;
	camera.lookAt(new THREE.Vector3(10, 0, 0));

	// add the output of the renderer to the html element
	document.querySelector("#bg").appendChild(webGLRenderer.domElement);

	// call the render function
	var step = 0;
	var spacer = 0;
	var minSpeed = 0.00025;
	var maxSpeed = 0.04;
	var speed = maxSpeed;

	var knot;
	
	function redraw() {
		if (knot){ scene.remove(knot); }	// remove old
		
		// create new
		//var geom = new THREE.TorusKnotGeometry(40, 28.2, 600, 12, 8, 6, 4);
		var geom = new THREE.TorusKnotGeometry(40, 28.2, 600, 12, 5, 4, 4);
		knot = createPoints(geom);
		
		scene.add(knot);
	}

	redraw();
	render();

	// from THREE.js examples
	function generateSprite() {

		var canvas = document.createElement('canvas');
		canvas.width = 16;
		canvas.height = 16;

		var context = canvas.getContext('2d');
		var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);
		gradient.addColorStop(0, 'rgba(255,255,255,1)');
		gradient.addColorStop(0.2, 'rgba(75,105,235,1)');
		gradient.addColorStop(0.4, 'rgba(0,0,64,1)');
		gradient.addColorStop(1, 'rgba(0,0,0,1)');

		context.fillStyle = gradient;
		context.fillRect(0, 0, canvas.width, canvas.height);

		var texture = new THREE.Texture(canvas);
		texture.needsUpdate = true;
		return texture;

	}

	function createPoints(geom) {
		var material = new THREE.PointCloudMaterial({
			color: 0xffffff,
			size: 3,
			transparent: true,
			blending: THREE.AdditiveBlending,
			map: generateSprite()
		});

		var system = new THREE.PointCloud(geom, material);
		system.sortParticles = true;
		return system;
	}

	function render() {
		if(speed > minSpeed){
			if(spacer % 17 == 0){
				speed = (speed - minSpeed) / (1+maxSpeed);
			}
			knot.rotation.y = step += speed;
		} else {
			knot.rotation.y = step += (minSpeed*1.25);
		}
		
		//Lets not run this forever, don't want to crash tabs.
		// Or maybe we do?
		/*
		if(spacer == 500000){ //5000
			minSpeed = 0;
			speed = 0;
			spacer++;
			requestAnimationFrame(render);
			webGLRenderer.render(scene, camera);
		} else if(spacer < 500000){*/
			spacer++;
			requestAnimationFrame(render);
			webGLRenderer.render(scene, camera);
		//}
	}

	function windowWidth(){
		return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	}
	function windowHeight(){
		return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	}
	function getWindowWidth(){
		if(isSmallScreen()){
			return windowWidth();
		} else {
			return windowWidth() - document.querySelector('header').clientWidth - 1;
		}
	}
	function getWindowHeight(){
		if(isSmallScreen()){
			return windowHeight() - document.querySelector('footer').clientHeight - 1;
		} else {
			return windowHeight();
		}
	}
})();
