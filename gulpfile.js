var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('gulp-cssnano');
var runSequence = require('run-sequence');
var concat = require('gulp-concat');
const minify = require('gulp-minify');

const dir = "MilkmanMattyUmb";

gulp.task('default', function() {
    return;
});

/* Stylesheet Related Tasks */
gulp.task('styles', function() {
    return gulp.src(dir+'/scss/*.*css')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(dir+'/css'));
});

gulp.task('autoprefixer', function () {
	var postcss      = require('gulp-postcss');
	var sourcemaps   = require('gulp-sourcemaps');
	var autoprefixer = require('autoprefixer');

	return gulp.src(dir+'/css/*.css')
		.pipe(sourcemaps.init())
		.pipe(postcss([ autoprefixer() ]))
		.pipe(gulp.dest(dir+'/css'));
});

gulp.task('minify', function() {
	return gulp.src(dir+'/css/*.css')
		.pipe(cssnano({
			autoprefixer: {
				browsers: 'last 2 version, IE 10'
			},
			reduceIdents: {
				keyframes: false
			},
			discardUnused: {
				keyframes: false
			}
		}))
		.pipe(gulp.dest(dir+'/css'));
});

gulp.task('buildcss', gulp.series('styles','autoprefixer','minify', function (done) {
    done();
}));

//Watch task
gulp.task('watch',function() {
    gulp.watch(dir+'/scss/*.scss', gulp.series('buildcss'));
    gulp.watch(dir+'/js/*.js', gulp.series('buildjs'));
});

gulp.task('compress_concat', function () {
	return gulp.src(dir+'/js/*.js')
		.pipe(minify())
		.pipe(gulp.dest(dir+'/js/min'))
});

gulp.task('buildjs', gulp.series('compress_concat', function (done) {
    done();
}));